using System;
using System.Runtime.InteropServices;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

using Microsoft.Win32.SafeHandles;

namespace DeviceIO
{
    /// <summary>
    /// 開放処理などをきちんと行うNotifyHandleのラッパークラス
    /// </summary>
    public class SafeNotifyHandle : SafeHandleZeroOrMinusOneIsInvalid
    {
        public SafeNotifyHandle()
            : base(true)
        {
        }

        protected override bool ReleaseHandle()
        {
            if( !this.IsInvalid )
                return Notification.UnregisterDeviceNotification(this.handle);
            return false;
        }
    }
    
    /// <summary>
    /// 通知の理由を表す
    /// </summary>
    public enum NotifyReason
    {
        /// <summary>
        /// 新しいデバイスが検出された
        /// </summary>
        DeviceArrival = 0x8000,
        /// <summary>
        /// 取り外しを要求された．失敗する可能性もある．
        /// </summary>
        QueryRemove = 0x8001,
        /// <summary>
        /// 取り外しを要求されたが，失敗した．
        /// </summary>
        QueryRemoveFailed = 0x8002,
        /// <summary>
        /// 取り外しの処理が進行中だが，まだ有効である．
        /// </summary>
        RemovePending = 0x8003,
        /// <summary>
        /// 取り外しの処理が完了した．
        /// </summary>
        RemoveComplete = 0x8004,
        /// <summary>
        /// 型特有の理由
        /// </summary>
        TypeSpecific = 0x8005,
        /// <summary>
        /// ユーザー定義の理由
        /// </summary>
        Custom = 0x8006,
    }

    /// <summary>
    /// DeviceChangeイベントの詳細を保持する
    /// </summary>
    public class DeviceChangeEventArgs : EventArgs
    {
        private NotifyReason reason;

        /// <summary>
        /// DeviceChangeイベントが起こった原因を取得する．
        /// </summary>
        public NotifyReason Reason
        {
            get { return this.reason; }
        }

        /// <summary>
        /// DeviceChangeEventArgsを構築する．
        /// </summary>
        /// <param name="reason">DeviceChangeイベントが起こった原因</param>
        public DeviceChangeEventArgs(NotifyReason reason)
        {
            this.reason = reason;
        }
    }

    /// <summary>
    /// デバイスに関する通知を受け取るクラス
    /// </summary>
    public class Notification : NativeWindow, IDisposable
    {
        internal enum NotificationRecipientFlags
        {
            DEVICE_NOTIFY_WINDOW_HANDLE = 0x00000000,
            DEVICE_NOTIFY_SERVICE_HANDLE = 0x00000001,
        }

        private const int DBT_DEVTYP_DEVICEINTERFACE = 0x00000005;
        private const int WM_DEVICECHANGE = 0x0219;

        
        [DllImport("user32", SetLastError = true)]
        internal static extern SafeNotifyHandle RegisterDeviceNotification(IntPtr hRecipient, IntPtr NotificationFilter, NotificationRecipientFlags Flags);
        [DllImport("user32", SetLastError = true)]
        internal static extern bool UnregisterDeviceNotification(IntPtr Handle);

        [StructLayout(LayoutKind.Sequential, Pack=1, CharSet = CharSet.Unicode)]
        struct DEV_BROADCAST_DEVICEINTERFACE 
        {
            public int dbcc_size;
            public int dbcc_devicetype;
            public int dbcc_reserved;
            public Guid dbcc_classguid;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst=255)]
            public string dbcc_name;
        }

        private SafeNotifyHandle handle;
        private Control parent;
        private Guid guid;

        /// <summary>
        /// 指定したGUIDのデバイスクラスに属するデバイスに関する変化を受け取るNotificationを構築する．
        /// </summary>
        /// <param name="parent">通知を受け取るためのウィンドウ</param>
        /// <param name="guid">デバイスクラスのGUID</param>
        /// <remarks>
        /// デバイスの変化に関する通知を受け取るには，メッセージループをまわす必要がある点に注意．
        /// </remarks>
        public Notification(Control parent, Guid guid)
        {
            this.parent = parent;
            this.guid = guid;
            parent.HandleCreated += new EventHandler(ParentHandleCreated);
            parent.HandleDestroyed += new EventHandler(ParentHandleDestroyed);
        }

        /// <summary>
        /// 指定したGUIDのデバイスクラスに属するデバイスに関する変化を受け取るNotificationを構築する．
        /// </summary>
        /// <param name="guid">デバイスクラスのGUID</param>
        /// <remarks>
        /// 通知を受け取るために必要なウィンドウを内部で作成しているので，各GUIDごとに共有することが望ましい．
        /// デバイスの変化に関する通知を受け取るには，メッセージループをまわす必要がある点に注意．
        /// </remarks>
        public Notification(Guid guid)
        {
            this.parent = null;
            this.guid = guid;

            CreateParams cp = new CreateParams();
            cp.Caption = "DeviceNotificationWindow";
            cp.ClassName = null;
            cp.ClassStyle = 0;
            cp.Style = 0;
            cp.ExStyle = 0;
            cp.Param = null;
            cp.Parent = IntPtr.Zero;
            cp.Width = 0;
            cp.Height = 0;
            this.CreateHandle(cp);

            this.RegisterNotification();
        }

        void ParentHandleDestroyed(object sender, EventArgs e)
        {
            this.Dispose();
        }

        void ParentHandleCreated(object sender, EventArgs e)
        {
            this.AssignHandle(this.parent.Handle);
            this.RegisterNotification();   
        }

        private void RegisterNotification()
        {
            DEV_BROADCAST_DEVICEINTERFACE filter = new DEV_BROADCAST_DEVICEINTERFACE();
            filter.dbcc_devicetype = DBT_DEVTYP_DEVICEINTERFACE;
            filter.dbcc_classguid = this.guid;
            filter.dbcc_size = Marshal.SizeOf(filter);
            filter.dbcc_name = "";
            GlobalBuffer filterPtr = new GlobalBuffer(filter.dbcc_size);
            Marshal.StructureToPtr(filter, filterPtr, true);
            using (filterPtr)
            {
                this.handle = RegisterDeviceNotification(this.Handle, filterPtr, NotificationRecipientFlags.DEVICE_NOTIFY_WINDOW_HANDLE);
            }
        }

        public event EventHandler<DeviceChangeEventArgs> DeviceChanged;
        protected void OnDeviceChanged(DeviceChangeEventArgs e)
        {
            if (DeviceChanged != null) DeviceChanged(this, e);
        }

        protected override void WndProc(ref Message m)
        {
            if (m.Msg != WM_DEVICECHANGE)
            {
                base.WndProc(ref m);
            }
            else
            {
                // デバイスの状態が変化した．
                int reason = m.WParam.ToInt32();
                NotifyReason notifyReason = (NotifyReason)Enum.ToObject(typeof(NotifyReason), reason);
                if(Enum.IsDefined(typeof(NotifyReason), notifyReason))
                    this.OnDeviceChanged(new DeviceChangeEventArgs(notifyReason));
                base.WndProc(ref m);
            }
        }

        #region IDisposable

        bool disposed = false;
        public void Dispose(bool disposing)
        {
            if (!disposed && disposing)
            {
                if (this.parent != null)
                {
                    this.ReleaseHandle();
                }
                if (!this.handle.IsInvalid)
                    this.handle.Close();
                disposed = true;
            }
        }

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        ~Notification()
        {
            this.Dispose(false);
        }
        #endregion
    }
}
